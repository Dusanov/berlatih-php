<?php
function tentukan_nilai($number) {

    if ($number >= 98) {
    	echo "Sangat Baik";

	} else if ($number < 80 && $number >= 70) {
		echo "Baik";
		
	} else if ($number < 70 && $number >= 60) {
		echo "Cukup";
		
	} else {
		echo "Kurang";
	}
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
?>