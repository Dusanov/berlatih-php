<?php
function ubah_huruf($abjad){
	$abjad = "abcdefghijklmnopqrstuvwxyz";
	$position = strpos($abjad, "w");
	echo substr($abjad, $position+1, 1);
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>